# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files for examples

# If running interactively, then:
if [ "$PS1" ]; then

    # Constants
    REGULAR="0"
    BOLD="1"
    RED="31"
    GREEN="32"
    YELLOW="33"
    BLUE="34"

    # Some defaults
    PROMPT_PREFIX=""
    PROMPT_TEXT="\u@\h "
    PROMPT_DIR_COLOR="$BOLD;$YELLOW"
    PROMPT_ROOT_COLOR="$BOLD;$RED"
    PROMPT_USER_COLOR="$BOLD;$GREEN"
    if [ "$UID" = "0" ] ; then
        PROMPT_COLOR=${PROMPT_ROOT_COLOR}
    else
        PROMPT_COLOR=${PROMPT_USER_COLOR}
    fi

    # Let user-provided file override the defaults
    if [ -f ~/.bashrc-defaults ] ; then . ~/.bashrc-defaults ; fi

    # Enable color support of ls and also add handy aliases
    eval `dircolors -b`
    alias ls='ls --color=auto -F '
    alias ll='ls -l --time-style=long-iso'
    alias l1='ls -1 --time-style=long-iso'
    alias la='ls -A'
    alias l='ls -CF'
    alias dir='ls --color=auto --format=vertical'
    alias vdir='ls --color=auto --format=long'

    alias mc='mc -X'   # make mc (Midnight Commander) start much faster

    alias rg='rg --colors "path:fg:yellow"'

    # set a fancy prompt
    if [ "$TERM" = "linux" ] ; then
        # At the console: don't set the term title
        PROMPT_TERM_TITLE=""
    else
        # Not at the console; set the term title to user@host + working directory
        PROMPT_TERM_TITLE="\[\e]0;\u@\h \w\007"
    fi
    PS1="\n${PROMPT_TERM_TITLE}\e[${PROMPT_COLOR}m\]${PROMPT_PREFIX}${PROMPT_TEXT}\e[${PROMPT_DIR_COLOR}m\w\e[${REGULAR}m\n\\$ "

    if [ -d ~/bin ] ; then
        PATH="~/bin:${PATH}"
    fi
    if [ -d ~/.local/bin ] ; then
        PATH="~/.local/bin:${PATH}"
    fi

    # include /sbin and /usr/sbin in path
    PATH="${PATH}:/usr/sbin:/sbin"

    if [ $(type -t lesspipe) = "file" ] ; then
        eval $(lesspipe)
    fi

    # Programmable completion
    if [ -e /etc/bash_completion ] ; then
        source /etc/bash_completion
    fi

    # Make ncurses use Unicode characters for line drawing instead of
    # ACS line drawing (fixes line drawing in aptitude)
    export NCURSES_NO_UTF8_ACS=1

    export VISUAL=vim
    export EDITOR=vim
    
    # Set up fzf
    [ -f ~/.fzf.bash ] && source ~/.fzf.bash

    # Call user-provided script for local customization
    if [ -f ~/.bashrc-post ] ; then . ~/.bashrc-post ; fi

fi
