switch (uname)
case Darwin
    function prompt_pwd_long --description "Long password prompt"
        echo $PWD | sed -e "s|^$HOME|~|" -e 's|^/private||'
    end
case 'CYGWIN_*'
    function prompt_pwd_long --description "Long password prompt"
        echo $PWD | sed -e "s|^$HOME|~|" -e 's|^/cygdrive/\(.\)|\1/:|'
    end
case '*'
    function prompt_pwd_long --description "Long password prompt"
        echo $PWD | sed -e "s|^$HOME|~|"
    end
end
